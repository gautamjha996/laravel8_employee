<?php
use App\Http\Controllers\AdminController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\CouponController;
use App\Http\Controllers\SizeController;
use App\Http\Controllers\ColorController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\CompanyController;
use App\Http\Controllers\EmployeeController;

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('admin',[AdminController::class,'index']);
Route::post('admin/auth',[AdminController::class,'auth'])->name('admin.auth');
Route::group(['middleware'=>'admin_auth'],function(){
Route::get('admin/dashboard',[AdminController::class,'dashboard']);
Route::get('admin/category',[CategoryController::class,'index']); 
Route::get('admin/category/manage_category',[CategoryController::class,'manage_category']); 
Route::get('admin/category/manage_category/{id}',[CategoryController::class,'manage_category']); 
Route::post('admin/category/manage_category_process',[CategoryController::class,'manage_category_process'])->name('category.manage_category_process'); 
Route::get('admin/category/delete/{id}',[CategoryController::class,'delete']); 
Route::get('admin/category/status/{status}/{id}',[CategoryController::class,'status']); 
//Route::get('admin/updatepassword',[AdminController::class,'updatepassword']); 
Route::get('admin/coupon',[CouponController::class,'index']); 
Route::get('admin/coupon/manage_coupon',[CouponController::class,'manage_coupon']); 
Route::get('admin/coupon/manage_coupon/{id}',[CouponController::class,'manage_coupon']); 
Route::post('admin/coupon/manage_coupon_process',[CouponController::class,'manage_coupon_process'])->name('coupon.manage_coupon_process'); 
Route::get('admin/coupon/delete/{id}',[CouponController::class,'delete']); 
Route::get('admin/coupon/status/{status}/{id}',[CouponController::class,'status']);
// size route---define
Route::get('admin/size',[SizeController::class,'index']); 
Route::get('admin/size/manage_size',[SizeController::class,'manage_size']); 
Route::get('admin/size/manage_size/{id}',[SizeController::class,'manage_size']); 
Route::post('admin/size/manage_size_process',[SizeController::class,'manage_size_process'])->name('size.manage_size_process'); 
// define color
Route::get('admin/color',[ColorController::class,'index']); 
Route::get('admin/color/manage_color',[ColorController::class,'manage_color']); 
Route::get('admin/color/manage_color/{id}',[ColorController::class,'manage_color']); 
Route::post('admin/color/manage_color_process',[ColorController::class,'manage_color_process'])->name('color.manage_color_process');
Route::get('admin/color/delete/{id}',[ColorController::class,'delete']); 
Route::get('admin/color/status/{status}/{id}',[ColorController::class,'status']);
// define product--//
Route::get('admin/product',[ProductController::class,'index']); 
Route::get('admin/product/manage_product',[ProductController::class,'manage_product']); 
Route::get('admin/product/manage_product/{id}',[ProductController::class,'manage_product']); 
Route::post('admin/product/manage_product_process',[ProductController::class,'manage_product_process'])->name('product.manage_product_process');
Route::get('admin/product/delete/{id}',[ProductController::class,'delete']); 
Route::get('admin/product/status/{status}/{id}',[ProductController::class,'status']);
//company define
Route::get('admin/company',[CompanyController::class,'index']); 
Route::get('admin/company/manage_company',[CompanyController::class,'manage_company']); 
Route::get('admin/company/manage_company/{id}',[CompanyController::class,'manage_company']); 
Route::post('admin/company/manage_company_process',[CompanyController::class,'manage_company_process'])->name('company.manage_company_process');
Route::get('admin/company/delete/{id}',[CompanyController::class,'delete']); 
Route::get('admin/company/status/{status}/{id}',[CompanyController::class,'status']);
//employee define---//
Route::get('admin/employee',[EmployeeController::class,'index']); 
Route::get('admin/employee/manage_employee',[EmployeeController::class,'manage_employee']); 
Route::get('admin/employee/manage_employee/{id}',[EmployeeController::class,'manage_employee']); 
Route::post('admin/employee/manage_employee_process',[EmployeeController::class,'manage_employee_process'])->name('employee.manage_employee_process');
Route::get('admin/employee/delete/{id}',[EmployeeController::class,'delete']); 
Route::get('admin/employee/status/{status}/{id}',[EmployeeController::class,'status']);
Route::get('admin/logout', function () {
   session()->forget('ADMIN_LOGIN');
   session()->forget('ADMIN_ID');
   session()->flash('error','Logout sucessfully ');
    return redirect('admin');
}); 
});

