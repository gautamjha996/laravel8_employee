 @extends('admin/layout');
 @section('page_title','Manage Company')
 @section('company_select','Active')
@section('container')
<div class="row m-t-30">
 <div class="col-lg-6">
                           
                                <div class="card" style="width:900px;">
                                   <a href="{{url('admin/company')}}"><button class="au-btn au-btn-icon au-btn--green au-btn--small">
                                            <i class="zmdi zmdi-plus"></i>Back</button></a>
                                    <div class="card-body">
                                        <div class="card-title">
                                            <h3 class="text-center title-2">create company</h3>
                                        </div>
                                        <hr>
                                        <form action="{{route('company.manage_company_process')}}" method="post" enctype="multipart/form-data">
                                           @csrf
                                            <div class="form-group has-success">
                                                <label for="cc-name" class="control-label mb-1">Name</label>
                                                <input id="name" name="name" type="text" class="form-control" value="{{$name}}" required>
                                                @error('name')
                                                     <div class="alert alert-danger" role="alert">
                                             {{$message}}
                                        </div>
                                                @enderror
                                            </div>
                                            <div class="form-group">
                                                <label for="cc-number" class="control-label mb-1">Email</label>
                                                <input id="email" name="email" type="text" class="form-control " value="{{$email}}">
                                              @error('email')
                                                
                                                  <div class="alert alert-danger" role="alert">
                                             {{$message}}
                                        </div>
                                                @enderror
                                            </div>
                                               <div class="form-group">
                                    <label for="image" class="control-label mb-1"> Logo</label>
                                    <input id="logo" name="logo" type="file" class="form-control" aria-required="true" aria-invalid="false">
                                    
                                      @if($logo!='')
                                   
                                       <img width="100px" src="{{asset('storage/media/company/'.$logo)}}"/>
                                        @endif
                                   
                                </div>
                                            <div>
                                                <button id="payment-button" type="submit" class="btn btn-lg btn-info btn-block">
                                                    
                                                   Submit
                                                </button>
                                            </div>
                                            <input type="hidden" name="id" value="{{$id}}">
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                            @endsection