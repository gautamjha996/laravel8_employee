 @extends('admin/layout');
 @section('page_title','Manage Employee')
@section('employee_select','Active')
@section('container')
<div class="row m-t-30">
 <div class="col-lg-6">
                           
                                <div class="card" style="width:900px;">
                                   <a href="{{url('admin/employee')}}"><button class="au-btn au-btn-icon au-btn--green au-btn--small">
                                            <i class="zmdi zmdi-plus"></i>Back</button></a>
                                    <div class="card-body">
                                        <div class="card-title">
                                            <h3 class="text-center title-2">create Employee</h3>
                                        </div>
                                        <hr>
                                        <form action="{{route('employee.manage_employee_process')}}" method="post" >
                                           @csrf
                                            <div class="form-group has-success">
                                                <label for="cc-name" class="control-label mb-1">Fname</label>
                                                <input id="first_name" name="first_name" type="text" class="form-control" value="{{$first_name}}" required>
                                                @error('first_name')
                                                     <div class="alert alert-danger" role="alert">
                                             {{$message}}
                                        </div>
                                                @enderror
                                            </div>
                                            <div class="form-group">
                                                <label for="cc-number" class="control-label mb-1">Lname</label>
                                                <input id="last_name" name="last_name" type="text" class="form-control " value="{{$last_name}}"  required>
                                              @error('last_name')
                                                
                                                  <div class="alert alert-danger" role="alert">
                                             {{$message}}
                                        </div>
                                                @enderror
                                            </div>
                                             <div class="form-group">
                                                <label for="cc-number" class="control-label mb-1">Company</label>
                                                <select id="company_id" name="company_id" class="form-control" required>
                                                 <option value="">Select Company</option>
                                                 @foreach(@$company as $list)
                                                 @if($company_id==$list->id)
                                                 <option selected value="{{$list->id}}">
                                                    @else
                                                 <option value="{{$list->id}}">
                                                    @endif
                                                    {{$list->name}}
                                                 </option>
                                                 @endforeach
                                              </select>  
                                            </div>
                                          <div class="form-group">
                                                <label for="cc-number" class="control-label mb-1">Email</label>
                                                <input id="email" name="email" type="text" class="form-control " value="{{$email}}"  required>
                                              @error('email')
                                                
                                                  <div class="alert alert-danger" role="alert">
                                             {{$message}}
                                        </div>
                                                @enderror
                                            </div>
                                               <div class="form-group">
                                                <label for="cc-number" class="control-label mb-1">Phone</label>
                                                <input id="phone" name="phone" type="text" class="form-control " value="{{$phone}}"  required>
                                              @error('phone')
                                                
                                                  <div class="alert alert-danger" role="alert">
                                             {{$message}}
                                        </div>
                                                @enderror
                                            </div>
                                            <div>
                                                <button id="payment-button" type="submit" class="btn btn-lg btn-info btn-block">
                                                    
                                                   Submit
                                                </button>
                                            </div>
                                            <input type="hidden" name="id" value="{{$id}}">
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                            @endsection