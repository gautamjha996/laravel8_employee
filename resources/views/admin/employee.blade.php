@extends('admin/layout');
@section('page_title','Employee')
@section('employee_select','Active')
@section('container')

                @if(session()->has('message'))
    <div class="sufee-alert alert with-close alert-success alert-dismissible fade show"> {{session('message')}}
                                            
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
</div>  
@endif 
<h1>Employee </h1>
<a href="{{url('admin/employee/manage_employee')}}"><button class="au-btn au-btn-icon au-btn--green au-btn--small">
                                            <i class="zmdi zmdi-plus"></i>Add Employee</button></a>
 <div class="row m-t-30">
                            <div class="col-md-12">
                                <!-- DATA TABLE-->
                                <div class="table-responsive m-b-40">
                                    <table class="table table-borderless table-data3">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Fname</th>
                                                <th>Lname</th>
                                                <th>Phone</th>
                                                <th>Company</th>
                                                <th>Action</th>
                                                
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($data as $list)
                                            <tr>
                                                <td>{{$list->id}}</td>
                                                <td>{{$list->first_name}}</td>
                                                <td>{{$list->last_name}}</td>
                                                <td>{{$list->phone}}</td>
                                                 <td>{{$list->name}}</td>
                                                <td><a href="{{url('admin/employee/manage_employee/')}}/{{$list->id}}"><button type="button" class="btn btn-success">Edit</button></a>
                                                     @if($list->status==1)
                                                    <a href="{{url('admin/employee/status/0')}}/{{$list->id}}"><button type="button" class="btn btn-primary">Active</button></a>
                                                    @elseif($list->status==0)
                                                     <a href="{{url('admin/employee/status/1')}}/{{$list->id}}"><button type="button" class="btn btn-warning">DeActive</button></a>
                                                    @endif
                                                    <a href="{{url('admin/employee/delete/')}}/{{$list->id}}"><button type="button" class="btn btn-danger">Delete</button></a></td>
                                            </tr>
                                           @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <!-- END DATA TABLE-->
                            </div>
                        </div>
@endsection