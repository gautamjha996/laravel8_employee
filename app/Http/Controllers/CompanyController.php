<?php

namespace App\Http\Controllers;

use App\Models\Company;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Storage;


class CompanyController extends Controller
{
    public function index()
    {
        $result['data']=Company::all();
        return view('admin/company',$result);
    }

    
    public function manage_company(Request $request,$id='')
    {
        if($id>0){
            $arr=Company::where(['id'=>$id])->get(); 

            $result['name']=$arr['0']->name;
            $result['email']=$arr['0']->email;
            $result['logo']=$arr['0']->logo;
            $result['status']=$arr['0']->status;
            $result['id']=$arr['0']->id;
        }else{
            $result['name']='';
            $result['email']="";
            $result['logo']='';
            $result['status']='';
            $result['id']=0;
            
        }
        return view('admin/manage_company',$result);
    }

    public function manage_company_process(Request $request)
    {
        //return $request->post();
        
        $request->validate([
            'name'=>'required|unique:companies,name,'.$request->post('id'), 
            'logo'=>'mimes:jpeg,jpg,png'
        ]);

        if($request->post('id')>0){
            $model=Company::find($request->post('id'));
            $msg="Company updated";
        }else{
            $model=new Company();
            $msg="Company inserted";
        }

        if($request->hasfile('logo')){

            if($request->post('id')>0){
                $arrImage=DB::table('companies')->where(['id'=>$request->post('id')])->get();
                if(Storage::exists('/public/media/company/'.$arrImage[0]->logo)){
                    Storage::delete('/public/media/company/'.$arrImage[0]->logo);
                }
            }

            $logo=$request->file('logo');
            $ext=$logo->extension();
            $logo_name=time().'.'.$ext;
            $logo->storeAs('/public/media/company',$logo_name);
            $model->logo=$logo_name;
        }
       
        $model->name=$request->post('name');
        $model->email=$request->post('email');
        $model->status=1;
        $model->save();
        $request->session()->flash('message',$msg);
        return redirect('admin/company');
        
    }

    public function delete(Request $request,$id){
        $model=Company::find($id);
        $model->delete();
        $request->session()->flash('message','Company deleted');
        return redirect('admin/company');
    }

    public function status(Request $request,$status,$id){
        $model=Company::find($id);
        $model->status=$status;
        $model->save();
        $request->session()->flash('message','Company status updated');
        return redirect('admin/company');
    }
}
