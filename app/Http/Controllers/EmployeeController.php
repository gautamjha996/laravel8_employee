<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EmployeeController extends Controller
{
     public function index()
    {
       // $result['data']=Employee::all();
         $result['data']=DB::table('employees')
        ->select('employees.*','companies.name')
        ->leftJoin('companies','companies.id','=','employees.company_id')
        ->get(); 
        return view('admin/employee',$result);
    }
      public function manage_employee(Request $request,$id='')
    {
       if($id>0){
        $arr=Employee::where(['id'=>$id])->get();
        $result['first_name']=$arr['0']->first_name;
        $result['last_name']=$arr['0']->last_name;
        $result['company_id']=$arr['0']->company_id;
         $result['email']=$arr['0']->email;
        $result['phone']=$arr['0']->phone;
        $result['id']=$arr['0']->id;
       }
       else{
        $result['first_name']='';
        $result['last_name']='';
         $result['company_id']='';
          $result['email']='';
         $result['phone']='';
        $result['id']=0;
       }
        $result['company']=DB::table('companies')->where(['status'=>1])->get();
        return view('admin/manage_employee',$result);
    }

    public function manage_employee_process(Request $request){
         $request->validate([
            'first_name'=>'required',
            'last_name'=>'required',
            'email'=>'required|unique:employees,email,'.$request->post('id'),
             
         ]);
         if($request->post('id')>0){
            $model=Employee::find($request->post('id'));
            $msg='Employee updated';
         }
         else{
         $model=new Employee();
          $msg='Employee inserted';
          }
         $model->first_name=$request->post('first_name');
         $model->last_name=$request->post('last_name');
         $model->company_id=$request->post('company_id');
         $model->email=$request->post('email');
         $model->phone=$request->post('phone');
         $model->status=1;
         $model->save();
         $request->session()->flash('message',$msg);
         return redirect('admin/employee');
    }
   public function delete(Request $request,$id){
    $model=Employee::find($id);
    $model->delete();
    $request->session()->flash('message','Employee Deleted');
     return redirect('admin/employee');
   }
   public function status(Request $request,$status,$id){
    $model=Employee::find($id);
    $model->status=$status;
    $model->save();
    $request->session()->flash('message','Employee Status updated');
     return redirect('admin/employee');
   }
}
